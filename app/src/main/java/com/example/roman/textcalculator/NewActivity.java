package com.example.roman.textcalculator;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by roman on 01.11.2017.
 */

public class NewActivity extends MainActivity {
    private TextView numberFirst;
    private TextView numberSecond;
    private TextView resultText;
    private Button backButton;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.two_activity_main);
        numberFirst = (TextView) findViewById(R.id.number_First);
        numberSecond = (TextView) findViewById(R.id.number_Second);
        backButton = (Button) findViewById(R.id.back_button);
        resultText = (TextView) findViewById(R.id.result);

        //Получение данных из первого Activity
        String first_number = getIntent().getStringExtra("FirstNumber");
        String second_number = getIntent().getStringExtra("SecondNumber");

        //Переменная хранящая результат
        String resultAmountOperands = "" + add(Integer.parseInt(first_number), Integer.parseInt(second_number));

        outputResult(first_number, checkOperand(second_number), resultAmountOperands);
    }

    /**
     * Метод выводит результат на экран
     * @param first_number - первый операнд
     * @param second_number - второй операнд
     * @param resultAmountOperands - результат
     */
    public void outputResult(String first_number, String second_number, String resultAmountOperands) {
        numberFirst.setText(first_number);
        numberSecond.setText(second_number);
        resultText.setText(resultAmountOperands);
    }

    /**
     * Метод проверяет второй операнд на наличие знака "минус", для расстановки скобок
     * @param operand - проверяемый операнд
     * @return
     */
    private String checkOperand(String operand) {
        if (Integer.parseInt(operand) < 0) return "(" + operand + ")";
        return operand;
    }


    /**
     * Метод выполянет сложение двух операндов
     * @param first_number - первый операнд
     * @param second_number - второй операнд
     * @return
     */
    private double add(double first_number, double second_number) {
        return first_number + second_number;
    }


    /**
     * Метод определяет нажатие кнопки "назад"
     * @param v
     */
    public void Back(View v){
        switch (v.getId()) {
            case R.id.back_button:
                Intent intent = new Intent(this, MainActivity.class);
                startActivity(intent);
                break;
            default:
                break;
        }
    }


}
