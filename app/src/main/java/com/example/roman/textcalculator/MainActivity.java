package com.example.roman.textcalculator;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity {

    private EditText firstNumber;
    private EditText secondNumber;
    private Button resultButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstNumber =(EditText)findViewById(R.id.firstTextField);
        secondNumber =(EditText)findViewById(R.id.secondTextField);
        resultButton =(Button) findViewById(R.id.resultButton);
    }


    /**
     * Метод определяет нажатия кнопки "сложить"
     * @param v
     */
    public void Send(View v){
        switch (v.getId()) {
            case R.id.resultButton:

                if (!checkOnButtonClick(v, firstNumber)) break;
                if (!checkOnButtonClick(v, secondNumber)) break;

                Intent intent = new Intent(MainActivity.this,NewActivity.class);

                //Отправка данных в NewActivity
                intent.putExtra("FirstNumber", firstNumber.getText().toString());
                intent.putExtra("SecondNumber", secondNumber.getText().toString());

                //Осуществляет переход в другое окно (NewActivity)
                startActivity(intent);
                break;
            default: break;
        }
    }

    /**
     * Метод проверяет поля на пустоту
     * @param view
     * @param digit
     * @return
     */
    public boolean checkOnButtonClick(View view, EditText digit) {
        String operand = digit.getText().toString();
        if (operand.length() == 0) {
            digit.setError("Поле не может быть пустым!");
            return false;
        }
        return true;
    }

}
